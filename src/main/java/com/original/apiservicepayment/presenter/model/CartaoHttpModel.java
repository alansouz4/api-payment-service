package com.original.apiservicepayment.presenter.model;

import com.original.apiservicepayment.core.CartaoEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CartaoHttpModel {

    private int id;
    @NotBlank
    @Size(min = 16, max = 16, message = "Cartão deve ter 16 números")
    private String numero;
    private Boolean ativado;
    private LocalDateTime dataHoraCriacao;
    private LocalDateTime dataHoraAtualizacao;
    private CartaoEnum tipo;
    private Long idCliente;
}
