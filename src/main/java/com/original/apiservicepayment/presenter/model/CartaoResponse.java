package com.original.apiservicepayment.presenter.model;

import com.original.apiservicepayment.core.CartaoEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CartaoResponse {

    private int id;
    private String numero;
    private Boolean ativar;
    private LocalDateTime dataHoraCriacao;
    private LocalDateTime dataHoraAtualizacao;
    private CartaoEnum tipo;
    private Long idCliente;
}
