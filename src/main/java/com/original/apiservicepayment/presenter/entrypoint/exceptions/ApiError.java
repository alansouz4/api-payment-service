package com.original.apiservicepayment.presenter.entrypoint.exceptions;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class ApiError {

    private final String code;
    private final String message;
}
