package com.original.apiservicepayment.presenter.entrypoint;

import com.original.apiservicepayment.core.entity.CartaoEntity;
import com.original.apiservicepayment.core.entity.annotation.NotMonitored;
import com.original.apiservicepayment.core.usecase.*;
import com.original.apiservicepayment.event.CardCreateEvent;
import com.original.apiservicepayment.presenter.mappers.CartaoHttpModelMapper;
import com.original.apiservicepayment.presenter.model.CartaoHttpModel;
import com.original.apiservicepayment.presenter.model.CartaoResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.stream.Collectors;

@Api(value="API Serviço de Cartão")
@RestController
@RequestMapping("/v1/cartoes")
public class CartaoEntrypoint {

    @Autowired
    private CriarCartaoUseCase criarCartaoUseCase;

    @Autowired
    private ObterCardByIdUseCase obterCardByIdUseCase;

    @Autowired
    private ObterTodosCardsUseCase obterTodosCardsUseCase;

    @Autowired
    private UpdateCardUseCase updateCardUseCase;

    @Autowired
    private DeletarCardUseCase deletarCardUseCase;

    @Autowired
    private ApplicationEventPublisher publisher;

    @ApiOperation(value = "Cria um cartão")
    @PostMapping
    public ResponseEntity<Void> salvarCartao(@Validated @RequestBody CartaoHttpModel cartaoHttpModel,
                                                        HttpServletResponse response) {
        CartaoEntity cartaoEntity = criarCartaoUseCase.salvarCartao(CartaoHttpModelMapper.toEntity(cartaoHttpModel));

        // Evento para enviar email
        this.publisher.publishEvent(new CardCreateEvent(cartaoEntity, response));

        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @NotMonitored
    @ApiOperation(value = "Obtem os cartões")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Sucesso")})
    @GetMapping
    public ResponseEntity<List<CartaoResponse>> obterCartoes(@RequestParam Integer limit, @RequestParam Integer offset){
        List<CartaoEntity> cartaoEntity = obterTodosCardsUseCase.getAll(limit, offset);

        return ResponseEntity.ok().body(cartaoEntity.stream()
                        .map(entity -> CartaoHttpModelMapper.toResponse(entity))
                        .collect(Collectors.toList()));
    }

    @ApiOperation(value = "Obtem um cartão por Id")
    @GetMapping("/{id}")
    public ResponseEntity<CartaoResponse> obterCartaoPorId(@PathVariable(name = "id") int id){
            CartaoEntity cartaoEntity = obterCardByIdUseCase.getById(id);
            if (cartaoEntity == null) {
                return ResponseEntity.noContent().build();
            }

            return ResponseEntity.ok().body(CartaoHttpModelMapper.toResponse(cartaoEntity));
    }

    @ApiOperation(value = "Atualiza um cartão")
    @PutMapping("/{id}")
    public ResponseEntity<CartaoResponse> atualizarCartao(@Validated @PathVariable(name = "id") int id, @RequestBody CartaoHttpModel cartaoHttpModel){

            CartaoEntity cartaoEntity = obterCardByIdUseCase.getById(id);
            if (cartaoEntity == null) {
                return ResponseEntity.noContent().build();
            }

            cartaoEntity.setId(cartaoHttpModel.getId());
            cartaoEntity = updateCardUseCase.upDate(cartaoEntity,id);

            return ResponseEntity.status(HttpStatus.OK).body(CartaoHttpModelMapper.toResponse(cartaoEntity));
    }

    @ApiOperation(value = "Remove um cartão")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> removerCartao(@PathVariable int id){

            if (obterCartaoPorId(id).equals(null)) {
                ResponseEntity.notFound().build();
            }

            deletarCardUseCase.delete(id);
            return ResponseEntity.status(204).body("");

    }
}
