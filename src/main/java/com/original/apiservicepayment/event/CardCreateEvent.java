package com.original.apiservicepayment.event;

import com.original.apiservicepayment.core.entity.CartaoEntity;

import javax.servlet.http.HttpServletResponse;

public class CardCreateEvent extends ResourceCreatedEvent<Integer, CartaoEntity> {

    private CartaoEntity cardCriado;

    public CardCreateEvent(CartaoEntity cartaoEntity, HttpServletResponse response) {
        super(cartaoEntity.getId(), cartaoEntity, response);
        this.cardCriado = cartaoEntity;
    }

    public CartaoEntity getCardCriado(){
        return cardCriado;
    }

}
