package com.original.apiservicepayment.dataprovider.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Cartão não encontrado!")
public class CardNotFoundException extends RuntimeException{
}
