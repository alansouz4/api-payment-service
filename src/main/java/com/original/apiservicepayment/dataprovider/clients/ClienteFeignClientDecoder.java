package com.original.apiservicepayment.dataprovider.clients;

import feign.Response;
import feign.codec.ErrorDecoder;

public class ClienteFeignClientDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new ErrorDecoder.Default();

    @Override
    public Exception decode(String s, Response response) {

        if (response.status() == 404) {
            return new InvalidClienteException();
        } else {
            return errorDecoder.decode(s, response);
        }
    }
}
