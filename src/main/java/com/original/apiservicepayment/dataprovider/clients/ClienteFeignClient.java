package com.original.apiservicepayment.dataprovider.clients;

import com.original.apiservicepayment.dataprovider.entity.Cliente;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Component
@FeignClient(name = "ms-cliente", configuration = ClienteFeignClientConfiguration.class)
public interface ClienteFeignClient {

    @GetMapping(value = "/v1/clientes/{id}")
    ResponseEntity<Cliente> obterClientePorId(@PathVariable Long id);
}
