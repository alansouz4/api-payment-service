package com.original.apiservicepayment.dataprovider.entity;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class Cliente {

    private Long id;
    private String nome;
    private String email;
    private String telefone;
}
