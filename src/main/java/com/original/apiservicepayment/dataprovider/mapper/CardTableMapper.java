package com.original.apiservicepayment.dataprovider.mapper;

import com.original.apiservicepayment.core.entity.CartaoEntity;
import com.original.apiservicepayment.dataprovider.entity.CartaoTable;
import com.original.apiservicepayment.dataprovider.entity.Cliente;

import java.util.ArrayList;
import java.util.List;

public class CardTableMapper {

    private CardTableMapper() {}

    public static CartaoTable toTable(CartaoEntity cartaoEntity){

        Cliente cliente = Cliente.builder()
                .id(cartaoEntity.getIdCliente())
                .build();

        CartaoTable cartaoTable = CartaoTable
                .builder()
                .id(cartaoEntity.getId())
                .numero(cartaoEntity.getNumero())
                .ativado(cartaoEntity.getAtivado())
                .dataHoraCriacao(cartaoEntity.getDataHoraCriacao())
                .dataHoraAtualizacao(cartaoEntity.getDataHoraAtualizacao())
                .tipo(cartaoEntity.getTipo())
                .idCliente(cliente.getId())
                .build();
        return cartaoTable;
    }

    public static CartaoEntity fromTable(CartaoTable cartaoTable){

        Cliente cliente = Cliente.builder()
                .id(cartaoTable.getIdCliente())
                .build();

        CartaoEntity cartaoEntity = CartaoEntity
                .builder()
                .id(cartaoTable.getId())
                .numero(cartaoTable.getNumero())
                .ativado(cartaoTable.getAtivado())
                .dataHoraCriacao(cartaoTable.getDataHoraCriacao())
                .dataHoraAtualizacao(cartaoTable.getDataHoraAtualizacao())
                .tipo(cartaoTable.getTipo())
                .idCliente(cliente.getId())
                .build();
        return cartaoEntity;
    }

    public static List<CartaoEntity> fromTableList(List<CartaoTable> cartaoTable){

        List<CartaoEntity> entities = new ArrayList<CartaoEntity>();

        for (CartaoTable cartaoTableList : cartaoTable){
            if (cartaoTableList != null) {
                CartaoEntity cartaoEntity = CartaoEntity
                        .builder()
                        .id(cartaoTableList.getId())
                        .numero(cartaoTableList.getNumero())
                        .ativado(cartaoTableList.getAtivado())
                        .dataHoraCriacao(cartaoTableList.getDataHoraCriacao())
                        .tipo(cartaoTableList.getTipo())
                        .build();

                entities.add(cartaoEntity);
            } else {
                cartaoTableList = null;
            }
        }

        return entities;
    }

}
