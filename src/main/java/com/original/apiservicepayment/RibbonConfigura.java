package com.original.apiservicepayment;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import org.springframework.context.annotation.Bean;

public class RibbonConfigura {

    @Bean
    public IRule getRule() {
        return new RandomRule();
    }
}
