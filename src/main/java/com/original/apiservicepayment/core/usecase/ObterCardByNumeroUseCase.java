package com.original.apiservicepayment.core.usecase;

import com.original.apiservicepayment.core.entity.CartaoEntity;
import com.original.apiservicepayment.core.gateway.CartaoGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.cloud.sleuth.annotation.SpanTag;
import org.springframework.stereotype.Component;

@Component
public class ObterCardByNumeroUseCase {

    private CartaoGateway cartaoGateway;

    @Autowired
    public ObterCardByNumeroUseCase(CartaoGateway cartaoGateway){
        this.cartaoGateway = cartaoGateway;
    }

    @NewSpan("busca-cartão-por-número")
    public CartaoEntity getByNumber(@SpanTag(key = "NUMERO") String number) {
        CartaoEntity cartaoEntity = cartaoGateway.getByNumber(number);
        return cartaoEntity;
    }
}
