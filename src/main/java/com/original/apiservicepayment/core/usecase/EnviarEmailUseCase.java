package com.original.apiservicepayment.core.usecase;

import com.original.apiservicepayment.emails.EmailCardSolicitation;
import com.original.apiservicepayment.event.CardCreateEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class EnviarEmailUseCase {

    private EmailCardSolicitation emailCardSolicitation;

    @Autowired
    public EnviarEmailUseCase(EmailCardSolicitation emailCardSolicitation){
        this.emailCardSolicitation = emailCardSolicitation;
    }

//    @Autowired
//    private EmailCardIssued emailCardIssued;

    @NewSpan("envia-email")
    @EventListener
    public void submitEmailCardSolicitation(CardCreateEvent cardCreateEvent) {
        emailCardSolicitation.executaEnvioEmailSemImpacto(cardCreateEvent.getCardCriado());
    }
}
