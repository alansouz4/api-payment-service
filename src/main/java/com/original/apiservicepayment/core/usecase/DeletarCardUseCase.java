package com.original.apiservicepayment.core.usecase;

import com.original.apiservicepayment.core.gateway.CartaoGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.stereotype.Component;

@Component
public class DeletarCardUseCase {

    private CartaoGateway cartaoGateway;

    @Autowired
    public DeletarCardUseCase(CartaoGateway cartaoGateway){
        this.cartaoGateway = cartaoGateway;
    }

    @NewSpan("deleta-cartão")
    public void delete(int id) {
        cartaoGateway.delete(id);
    }
}
