package com.original.apiservicepayment.core.usecase;

import com.original.apiservicepayment.core.entity.CartaoEntity;
import com.original.apiservicepayment.core.gateway.CartaoGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.cloud.sleuth.annotation.SpanTag;
import org.springframework.stereotype.Component;

@Component
public class ObterCardByIdUseCase {

    private CartaoGateway cartaoGateway;

    @Autowired
    public ObterCardByIdUseCase(CartaoGateway cartaoGateway){
        this.cartaoGateway = cartaoGateway;
    }

    @NewSpan("busca-cartão-por-id")
    public CartaoEntity getById(@SpanTag(key = "ID") int id) {
        CartaoEntity cardOptional = cartaoGateway.getById(id);
        return cardOptional;
    }
}
