package com.original.apiservicepayment.core.usecase;

import com.original.apiservicepayment.core.entity.CartaoEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.stereotype.Component;

@Component
public class AtivarCardUseCase {

    @Autowired
    private ObterCardByNumeroUseCase obterCardByNumeroUseCase;

    @Autowired
    public AtivarCardUseCase(ObterCardByNumeroUseCase obterCardByNumeroUseCase){
        this.obterCardByNumeroUseCase = obterCardByNumeroUseCase;
    }

    @NewSpan("ativa-cartão")
    public CartaoEntity cardActive(CartaoEntity cartaoEntity) {
        CartaoEntity cartaoEntityDB = obterCardByNumeroUseCase.getByNumber(cartaoEntity.getNumero());
        cartaoEntityDB.setAtivado(cartaoEntity.getAtivado());
        return cartaoEntity;
    }
}
