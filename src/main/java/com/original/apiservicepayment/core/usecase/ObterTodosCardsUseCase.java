package com.original.apiservicepayment.core.usecase;

import com.original.apiservicepayment.core.entity.CartaoEntity;
import com.original.apiservicepayment.core.gateway.CartaoGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ObterTodosCardsUseCase {

    private CartaoGateway cartaoGateway;

    @Autowired
    public ObterTodosCardsUseCase(CartaoGateway cartaoGateway){
        this.cartaoGateway = cartaoGateway;
    }

    @NewSpan("busca-cartões")
    public List<CartaoEntity> getAll(Integer limit, Integer offset) {
        return cartaoGateway.getAll(limit, offset);
    }
}
