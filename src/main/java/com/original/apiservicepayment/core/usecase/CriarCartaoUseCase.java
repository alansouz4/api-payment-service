package com.original.apiservicepayment.core.usecase;

import com.original.apiservicepayment.configuration.exception.BusinessException;
import com.original.apiservicepayment.core.entity.CartaoEntity;
import com.original.apiservicepayment.core.gateway.CartaoGateway;
import org.hibernate.secure.spi.IntegrationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.cloud.sleuth.annotation.SpanTag;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class CriarCartaoUseCase {

    private CartaoGateway cartaoGateway;

    @Autowired
    public CriarCartaoUseCase(CartaoGateway cartaoGateway){
        this.cartaoGateway = cartaoGateway;
    }

    @NewSpan("cria-cartão")
    public CartaoEntity salvarCartao(@SpanTag(key = "CARTÃO") CartaoEntity cartaoEntity) {
        try{
            cartaoEntity.setAtivado(false);
            cartaoEntity.setDataHoraCriacao(LocalDateTime.now());

        } catch (IntegrationException e) {
            throw new BusinessException("Erro na criação");
        }
        return cartaoGateway.salvarCartao(cartaoEntity);
    }
}
