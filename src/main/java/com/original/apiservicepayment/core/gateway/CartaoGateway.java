package com.original.apiservicepayment.core.gateway;

import com.original.apiservicepayment.core.entity.CartaoEntity;

import java.util.List;

public interface CartaoGateway {

    CartaoEntity salvarCartao(CartaoEntity cartaoEntity);
    CartaoEntity cardActive(String number);
    CartaoEntity getByNumber(String number);
    CartaoEntity getById(long id);
    List<CartaoEntity> getAll(Integer limit, Integer offset);
    CartaoEntity upDate(CartaoEntity cartaoEntity, long id);
    String delete(long id);
}
