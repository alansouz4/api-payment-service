package com.original.apiservicepayment;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.ribbon.RibbonClients;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
@EnableEurekaClient
@RibbonClients(defaultConfiguration = RibbonConfigura.class)
public class ApiServicePaymentApplication {

	private static Logger logger = LoggerFactory.getLogger(ApiServicePaymentApplication.class);

	public static void main(String[] args) {
		logger.info("[API Serviço de Cartão] | Iniciando a api de pagamentos!");
		SpringApplication.run(ApiServicePaymentApplication.class, args);
		logger.info("[API Serviço de Cartão] | API de pagamentos iniciada e pronta para receber requisições!");
	}

}
