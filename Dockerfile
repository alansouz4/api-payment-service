FROM openjdk

WORKDIR /api-service-payment

COPY target/api-service-payment*.jar api-service-payment.jar

EXPOSE 9090

CMD ["java", "-jar", "api-service-payment.jar"]